import numpy as np
import matplotlib.pyplot as plt
from memb_func import *
from TrapzFuzzySet import TrapzFuzzySet


class FuzzyInferenceMachine:

    def __init__(self, rules_base):

        """
        Constructor of a FuzzyInferenceMachine object. Represents an inference machine
        which performs the processes of fuzzyfication, application of the fuzzy rules base,
        obtention of the output fuzzy set and defuzzyfication.

        :param rules_base: m x n array of TrapzFuzzySet objects. Represents a rules base of
        m rules and n variables of input and output.
        """

        self.RulesBase = rules_base

    def fuzzyfication(self, E1, E2):

        """
        Performs the fuzzyfication phase of the inputs E1, E2.

        :param E1: float. Input 1.
        :param E2: float. Input 2.
        :return: 2 8D Numpy arrays. Every array represents the degrees of membership of the
            numeric input to all the corresponding fuzzy sets.
        """
        # initialization of the output arrays
        mu_E1 = np.zeros(len(self.RulesBase))
        mu_E2 = np.zeros(len(self.RulesBase))

        for i in range(len(self.RulesBase)):

            # extraction of input fuzzy sets in the rule i
            (E1_fuzzy_set, E2_fuzzy_set) = (self.RulesBase[i][0], self.RulesBase[i][1])

            # calculation of degree of membership
            #mu_E1[i] = E1_fuzzy_set.calc_deg_m(E1, cut_value=1.0)
            #mu_E2[i] = E2_fuzzy_set.calc_deg_m(E2, cut_value=1.0)
            mu_E1[i] = E1_fuzzy_set.calc_deg_m(E1)
            mu_E2[i] = E2_fuzzy_set.calc_deg_m(E2)

        return mu_E1, mu_E2

    def input_intersection(self, mu_E1, mu_E2):

        """
        Performs the application of the norm T between the 2 inputs using their degrees of membership for
        every rule. Norm T used is the min function.

        :param mu_E1: 8D Numpy array. Degrees of membership of input E1 to their corresponding fuzzy sets.
        :param mu_E2: 8D Numpy array. Degrees of membership of input E2 to their corresponding fuzzy sets.
        :return: 8D Numpy array. Have all the values who cut the output fuzzy sets of every fuzzy rule.
        """

        # initialization of output vector
        cut_values = np.zeros(len(self.RulesBase))

        for i in range(len(self.RulesBase)):

            cut_values[i] = min(mu_E1[i], mu_E2[i])  # applying norm T: min, for every rule

        return cut_values

    def cut_output_fuzzy_sets(self, cut_values):

        """
        Calculates the output fuzzy sets for every rule after application of norm T, which membership
        functions are cut by the degrees of membership in cut_values.

        :param cut_values: 8D Numpy array. Stores the degrees of membership who cut their respective
            original functions.
        :return: Array of TrapzFuzzySet objects. Stores the necessary cut output fuzzy sets for the
            application of conorm T. Also returns active_rules, array of integers, who stores which rules
            were activated (cut value > 0).
        """

        # initialization of output arrays
        cut_fuzzy_sets = []
        active_rules = []

        for i in range(len(self.RulesBase)):

            if cut_values[i] == 1.0:  # all the output fuzzy set is available

                cut_fuzzy_sets.append(self.RulesBase[i][2])
                active_rules.append(i)

            elif 0.0 < cut_values[i] < 1.0:  # the output fuzzy set will be cut

                old_output_set = self.RulesBase[i][2]
                new_pars = cut_memb_func(old_output_set.Pars, cut_values[i])  # new cut membership function
                cut_fuzzy_sets.append(TrapzFuzzySet(new_pars, old_output_set.name + ' (cut output)'))
                active_rules.append(i)

        return cut_fuzzy_sets, active_rules

    def final_output_fuzzy_set(self, mu_E1, mu_E2):

        """
        Calculates the union of output fuzzy sets of all the fuzzy rules after application of norm T,
        generating a new membership function to be used in the defuzzyfication phase. In other words,
        applies the conorm T between the output fuzzy sets. Conorm T used is the max function.

        :param mu_E1: 8D Numpy array. Degrees of membership of input E1 to their corresponding fuzzy sets.
        :param mu_E2: 8D Numpy array. Degrees of membership of input E2 to their corresponding fuzzy sets.
        :return: Numpy array. Have the values of the new membership function (sampled in 100 equidistant points)
            in the range [-1, 1].
        """

        # initialization for final membership function
        x = np.linspace(-1.0, 1.0, 100)  # discrete interval for sampling of membership functions
        final_membership_function = np.zeros(100)

        # calculating cut values for output fuzzy sets
        cut_values = self.input_intersection(mu_E1, mu_E2)

        # cutting output fuzzy sets
        cut_fuzzy_sets, active_rules = self.cut_output_fuzzy_sets(cut_values)

        # calculating degrees of membership and applying conorm T: max
        for i in range(len(x)):

            for j in range(len(cut_fuzzy_sets)):

                # calc degree of membership for one output fuzzy set
                output_deg = cut_fuzzy_sets[j].calc_deg_m(x[i], cut_value=cut_values[active_rules[j]])

                # application of conorm T
                final_membership_function[i] = max(final_membership_function[i], output_deg)

        return final_membership_function

    def deffuzyfication(self, final_membership_function):

        """
        Performs the defuzzyfication phase using the membership function of the final output fuzzy set.
        The defuzzyfication method used is Center of Gravity (COG).

        :param final_membership_function: 100D Numpy array. Have values of the
            membership function of the final output fuzzy set.
        :return: float. Non fuzzy output of the inference machine.
        """

        # applying COG method (integration by trapezoidal rule)
        x = np.linspace(-1.0, 1.0, 100)  # sampled interval for discrete integration
        int_1 = np.trapz(final_membership_function * x, x)
        int_2 = np.trapz(final_membership_function, x)

        # calculating output

        # case when membership function is an horizontal line (constant value > 0)
        if max(final_membership_function) == min(final_membership_function):

            final_output = 0

        else:

            final_output = int_1/int_2

        return final_output
