import numpy as np
import matplotlib.pyplot as plt
from memb_func import memb_func

fuzzy_sets_names = {'ng': 'Negativo Grande',
                    'np': 'Negativo Pequeño',
                    'ce': 'Cero',
                    'pp': 'Positivo Pequeño',
                    'pg': 'Positivo Grande',
                    'de_a_ng_np': 'de_a ng np',
                    'de_a_np_pg': 'de_a np pg',
                    'de_a_pp_pg': 'de_a pp pg',
                    'de_a_ng_pp': 'de_a ng pp',
                    'de_a_ce_pp': 'de_a ce pp'}


class TrapzFuzzySet:

    def __init__(self, pars, name):

        """
        Constructor of a TrapzSet object, which represents a fuzzy set with trapezoidal membership function.

        :param pars: 4D Numpy array. Represents membership function of the fuzzy set.
        :param name: String. Name of the fuzzy set.
        """

        self.Pars = pars
        self.name = name

    def calc_deg_m(self, x, cut_value=1.0):

        """
        Uses the function memb_func to calculate the degree of membership of x
        with respect to the fuzzy set described by self.Pars.

        :param x: float. Input to the fuzzyfication phase.
        :param cut_value: float. For use when the membership function is limited by a degree
            of membership < 1. If that limit doesn't exists, set the value to 1.
        :return: float. Degree of membership with respect to the fuzzy set.
        """

        return memb_func(self.Pars, x, cut_value)

    def de_a(self, other_set):

        """
        Perform the union of intervals (the de_a operation) between 2 fuzzy sets at the level of membership functions,
        according to the enunciate.

        :param other_set: TrapzFuzzySet object. The other fuzzy set to be operated in the de_a operation.
        :return: TrapzFuzzySet object. The union of the 2 sets, thus, a new fuzzy set.
        """

        a1, b1, c1, d1 = self.Pars  # extract 1st parameters array
        a2, b2, c2, d2 = other_set.Pars  # extract 2nd parameters array
        union = np.zeros(4)  # initialization of array

        # 1st part of the new set: the minimum values of a and b
        union[0] = min(a1, a2)
        union[1] = min(b1, b2)

        # 2st part of the new set: the maximum values of c and d
        union[2] = max(c1, c2)
        union[3] = max(d1, d2)

        return TrapzFuzzySet(union, 'de_a_' + self.name + '_' + other_set.name)

    def plot_fuzzy_set(self):

        """
        Plots the membership function of the fuzzy set.
        """
        degs = np.array([0.0, 0.0, 1.0, 1.0, 0.0, 0.0])
        x_array = np.zeros(degs.shape[0])
        x_array[0] = self.Pars[0] - 0.2
        x_array[1:5] = self.Pars
        x_array[5] = self.Pars[3] + 0.2
        plt.plot(x_array, degs, '.-', mew=3)
        plt.xlabel('x')
        plt.ylabel('Grado de Pertenencia')
        name = fuzzy_sets_names[self.name] if self.name in fuzzy_sets_names.keys() else self.name
        plt.title('Conjunto ' + name)
        x_inf_lim = x_array[0] if x_array[0] > -1.0 else -1.0
        x_sup_lim = x_array[5] if x_array[5] < 1.0 else 1.0
        plt.xlim(x_inf_lim, x_sup_lim)
        plt.xticks(np.arange(x_inf_lim, x_sup_lim + 0.2, step=0.2))
        plt.grid()
        plt.savefig('Results/' + self.name + '.png', format='png', dpi=1200)
        plt.show()

    def plot_multiple_sets(self, sets, figname):

        """
        Plots multiple membership functions in the same plot.

        :param sets: List of TrapzFuzzySet objects.
        :param figname: String. Name of the plot to be exported as .png.
        """

        # plot membership function of self object
        degs = np.array([0.0, 0.0, 1.0, 1.0, 0.0, 0.0])
        x_array = np.zeros(degs.shape[0])
        x_array[0] = -1.0
        x_array[1:5] = self.Pars
        x_array[5] = 1.0
        plt.plot(x_array, degs, '.-', mew=3, label=self.name)

        # plot the other membership functions of the fuzzy sets in the list
        for set in sets:

            x_array[1:5] = set.Pars
            plt.plot(x_array, degs, '.-', mew=3, label=set.name)

        plt.xlabel('x')
        plt.ylabel('Grado de Pertenencia')
        plt.title('Conjuntos difusos')
        plt.xlim(-1.0, 1.0)
        plt.xticks(np.arange(-1, 1.2, step=0.2))
        plt.legend(loc='center right')
        plt.grid()
        plt.savefig('Results/' + figname + '.png', format='png', dpi=1200)
        plt.show()
