# Tarea 1 EL7038 2021-1

Implementación en código de la tarea de avance 1 del curso EL7038 Introducción a la Teoría de Conjuntos Difusos y Sistemas Inteligentes, que consiste en una máquina de inferencia difusa. Esto puede usarse como base para el desarrollo de un controlador difuso/sistema híbrido que use lógica difusa.
