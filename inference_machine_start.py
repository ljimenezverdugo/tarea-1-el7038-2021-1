from def_fuzzy_sets import ng_set, np_set, ce_set, pp_set, pg_set, de_a_ng_np, \
    de_a_np_pg, de_a_pp_pg, de_a_ng_pp  # import fuzzy sets
from FuzzyInferenceMachine import FuzzyInferenceMachine
import numpy as np
import matplotlib.pyplot as plt

"""
Creating fuzzy inference machine with the rules base of the enunciate:

- rules_base[i] represents the fuzzy rule i.
- rules_base[i][0] and rules_base[i][1] are the fuzzy sets for input
E1 and E2, respectively, in the fuzzy rule i.
- rules_base[i][2] are the fuzzy set for the output in the fuzzy rule i.
"""
rules_base = [[de_a_ng_np, de_a_ng_np, pg_set],
              [de_a_np_pg, ng_set, pg_set],
              [de_a_pp_pg, np_set, pp_set],
              [ce_set, np_set, ce_set],
              [ce_set, pp_set, ce_set],
              [de_a_ng_np, pp_set, np_set],
              [de_a_ng_pp, ng_set, ng_set],
              [de_a_pp_pg, de_a_pp_pg, ng_set]]

inference_machine = FuzzyInferenceMachine(rules_base)

# setting inputs
E1 = -0.4
E2 = np.linspace(-1.0, 1.0, 100)
#E1 = np.linspace(-1.0, 1.0, 100)
#E2 = 0.7

# initialization of final output
final_output = np.zeros(100)

# inference machine start!!
for i in range(len(E2)):
#for i in range(len(E1)):

    # fuzzyfication phase
    mu_E1, mu_E2 = inference_machine.fuzzyfication(E1, E2[i])
    #mu_E1, mu_E2 = inference_machine.fuzzyfication(E1[i], E2)

    # calculating union of all cut output fuzzy sets (membership function)
    final_membership_function = inference_machine.final_output_fuzzy_set(mu_E1, mu_E2)

    # defuzzyfication phase
    final_output[i] = inference_machine.deffuzyfication(final_membership_function)

# Plotting non fuzzy outputs of the fuzzy inference machine
plots = 0

if plots == 1:
    plt.plot(E1, final_output, '.-', mew=2)
    plt.xlabel('E1')
    plt.ylabel('Salida no difusa')
    plt.title('Salida no difusa para E2 = 0,7 y E1 = [-1,1]')
    plt.xlim(-1, 1)
    plt.xticks(np.arange(-1, 1.2, step=0.2))
    plt.grid()
    plt.savefig('Results/' + 'output_E2_fijo.png', format='png', dpi=1200)
    plt.show()

else:

    plt.plot(E2, final_output, '.-', mew=2)
    plt.xlabel('E2')
    plt.ylabel('Salida no difusa')
    plt.title('Salida no difusa para E1 = -0,4 y E2 = [-1,1]')
    plt.xlim(-1, 1)
    plt.xticks(np.arange(-1, 1.2, step=0.2))
    plt.grid()
    plt.savefig('Results/' + 'output_E1_fijo.png', format='png', dpi=1200)
    plt.show()
