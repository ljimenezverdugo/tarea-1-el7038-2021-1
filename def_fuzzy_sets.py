import numpy as np
from TrapzFuzzySet import TrapzFuzzySet

# Creating fuzzy sets
ng_array = np.array([-1.0, -1.0, -0.8, -0.6])
np_array = np.array([-0.8, -0.6, -0.2, 0.0])
ce_array = np.array([-0.2, 0.0, 0.0, 0.2])
pp_array = np.array([0.0, 0.2, 0.6, 0.8])
pg_array = np.array([0.6, 0.8, 1.0, 1.0])

ng_set = TrapzFuzzySet(ng_array, 'ng')
np_set = TrapzFuzzySet(np_array, 'np')
ce_set = TrapzFuzzySet(ce_array, 'ce')
pp_set = TrapzFuzzySet(pp_array, 'pp')
pg_set = TrapzFuzzySet(pg_array, 'pg')

# Perform de_a operation between some sets
de_a_ng_np = ng_set.de_a(np_set)
de_a_np_pg = np_set.de_a(pg_set)
de_a_pp_pg = pp_set.de_a(pg_set)
de_a_ng_pp = ng_set.de_a(pp_set)
