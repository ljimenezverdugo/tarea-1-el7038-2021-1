import numpy as np


def memb_func(pars, x, cut_value):

    """
    Calculates the degree of membership of a number x with respect to a fuzzy set,
    which membership function is described by the array pars.
    :param pars: 4D Numpy array. Defines the trapezoidal form of the membership function.
    :param x: float. Number to be fuzzyfied in the fuzzyfication phase.
    :param cut_value: float. For use when the membership function is limited by a degree
        of membership < 1.0. If that limit doesn't exists, set the value to 1.
    :return: float. Degree of membership of x with respect to the fuzzy set.
    """

    a, b, c, d = pars  # assumption: a <= b <= c <= d
    assert a <= b <= c <= d
    deg = 0.0  # initialization

    #  cases of possible functions

    #  triangular function
    if b == c:

        if x <= a:

            deg = 0.0

        elif a <= x <= b:

            deg = (x - a)/(b - a)

        elif b <= x <= d:

            deg = (d-x) / (d - b)

        else:

            deg = 0.0

    #  trapezoidal function (rect by left)
    elif a == b:

        if x < a:

            deg = 0.0

        elif a <= x <= c:

            deg = 1.0

        elif c <= x <= d:

            deg = (d-x) / (d - c)

        else:

            deg = 0.0

    #  trapezoidal function (rect by right)
    elif c == d:

        if x < a:

            deg = 0.0

        elif a <= x <= b:

            deg = (x - a)/(b - a)

        elif b <= x <= c:

            deg = 1.0

        else:

            deg = 0.0

    else:  # all parameters are different

        if x <= a:

            deg = 0.0

        elif a <= x <= b:

            deg = (x - a)/(b - a)

        elif b <= x <= c:

            deg = 1.0

        elif c <= x <= d:

            deg = (d - x)/(d - c)

        else:

            deg = 0.0

    if cut_value != 1.0:

        return deg if deg < cut_value else cut_value

    else:

        return deg


def cut_memb_func(pars, cut_value):

    """
    Cuts the membership function of the fuzzy set described by pars using the degree
    of membership cut_value, and calculates the parameters of the new membership function (a
    new fuzzy set).

    :param pars: 4D Numpy array. Defines the trapezoidal form of the membership function.
    :param cut_value: float. Degree of membership who cuts the original membership function.
    :return: 4D Numpy array, with the parameters of the new membership function.
    """

    a, b, c, d = pars  # assumption: a <= b <= c <= d
    assert a <= b <= c <= d
    new_pars = np.zeros(4)  # initialization
    (new_pars[0], new_pars[3]) = (a, d)  # the inf and sup limits are the same in the new membership function

    # point between a and b
    new_pars[1] = (b - a)*cut_value + a

    # point between c and d
    new_pars[2] = -(d - c)*cut_value + d

    return new_pars
