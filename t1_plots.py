from def_fuzzy_sets import ng_set, np_set, ce_set, pp_set, pg_set,\
    de_a_ng_np, de_a_np_pg, de_a_pp_pg, de_a_ng_pp  # import fuzzy sets


# Plotting all membership functions in one plot
ng_set.plot_multiple_sets([np_set, ce_set, pp_set, pg_set], figname='t1_a')

# Plotting membership functions in different plots
ng_set.plot_fuzzy_set()
np_set.plot_fuzzy_set()
ce_set.plot_fuzzy_set()
pp_set.plot_fuzzy_set()
pg_set.plot_fuzzy_set()

# Plotting membership functions of de_a fuzzy sets
de_a_ng_np.plot_fuzzy_set()
de_a_np_pg.plot_fuzzy_set()
de_a_pp_pg.plot_fuzzy_set()
de_a_ng_pp.plot_fuzzy_set()
#de_a_ce_pp.plot_fuzzy_set()

